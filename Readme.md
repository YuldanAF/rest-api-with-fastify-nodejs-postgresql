## _REST API CRUD WITH FASTIFY AND POSTGRESQL_


Development by Yuldan Angga Fahrosa.

## Tech




- [node.js] - evented I/O for the backend
- [Fastify] - fast node.js network app framework 
- [Postgresql] - For database storage


## Prepare

- Create database with zeroone.sql
## Feature
| Description | README |
| ------ | ------ |
| Get All Data News | http://localhost:3000/api/getallnews/ |
| Insert Data News | [http://localhost:3000/api/insertnews/ |
| Update Data News by id | [http://localhost:3000/api/updatenews/:id|
| Delete Data News by id | [http://localhost:3000/api/deletenews/:id|
| Get All Data Topics | [http://localhost:3000/api/getallcategory/|
| Insert Data Topics | [http://localhost:3000/api/insertcategory/|
| Update Data Topics | http://localhost:3000/api/updatecategory/:id |
| Delete Data Topics | [http://localhost:3000/api/deletecategory/:id |
| Get News By Id | [http://localhost:3000/api/getnewsbyid/:id |
| Get Topics By Id | [http://localhost:3000/api/getcategorybyid/:id|
| Get Relation Post Between News & Topics | [http://localhost:3000/api/getallpost/|
| Insert Relation Post Between News & Topics | [http://localhost:3000/api/insertpost/|
|Update Relation Post Between News & Topics | http://localhost:3000/api/updatepost/:id |
| Delete Relation Post Between News & Topics | [http://localhost:3000/api/deletepost/:id |
| Get Relation Post Between News & Topics By Id| [http://localhost:3000/api/getpostbyid/:id |
| Get Full Entity With Parameter status & topic | [http://localhost:3000/api/findallwithparam/:status/:topic|
| Publish Draft News | [http://localhost:3000/api/publish/:id|
| Move News Expired to Trash | [http://localhost:3000/api/trash/:id|

## Parameter
use parameter with body raw json

News Parameter :
```sh
{
    "title":"yourtitle",
    "content":"yourcontent",
    "writer":"yuldanaf"
}
```
Topic Parameter :
```sh
{
    "topic":"yourtopic",
    "ctype":"typetopic",
    "writer":"yuldanaf"
}
```

Relation Parameter :
```sh
{
    "idarticle":"articleid",
    "idtopic":"topicid"
}
```
## Run
```sh
--if not install node modules pls run script below 
npm instal
--and lets enjoy the rest with script below :)
npm start
```

> Note: `uuidv4` is use for generate all of primary.
