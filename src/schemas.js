const allNews = {
    response: {
        200: {
            type: 'array',
            items: {
                type: 'object',
                required: ['id', 'title', 'writer', 'content', 'createdate', 'status'],
                properties: {
                    id: {type: 'string',  format: 'uuid'},                                                              
                    title: {type: 'string'},
                    writer: {type: 'string'},
                    content: {type: 'string'},                                           
                    createdate:{type: 'string',format: "date-time"},                  
                    status: {type: 'string'},
                }
            }
        }
    }
}
const addNews = {
    body: {
        type: 'object',
        required: ['title','writer','content'],
        properties: {
            title: {type: 'string'},
            writer: {type: 'string'},
            content: {type: 'string'},                                           
            createdate:{type: 'string',format: "date-time"},                  
            status: {type: 'string'},
        }
    },

}
const updateNews = {
    body: {
        type: 'object',
        properties: {
            title: {type: 'string'},
            writer: {type: 'string'},
            content: {type: 'string'},
        }
    },
    params: {
        type: 'object',
        properties: {
          id: { type: 'string', format: 'uuid' }
        }
    }
}

const deleteNews = {
    params: {
        type: 'object',
        properties: {
            id: {type: 'string', format: 'uuid'}
        }
    }
}
const allCategory = {
    response: {
        200: {
            type: 'array',
            items: {
                type: 'object',
                required: ['id', 'topic', 'ctype', 'createdate'],
                properties: {
                    id: {type: 'string',  format: 'uuid'},                                                              
                    topic: {type: 'string'},
                    ctype: {type: 'string'},                                       
                    createdate:{type: 'string',format: "date-time"},                  
                }
            }
        }
    }
}
const addCategory = {
    body: {
        type: 'object',
        required: ['topic','ctype'],
        properties: {
            topic: {type: 'string'},
            ctype: {type: 'string'},                                       
            createdate:{type: 'string',format: "date-time"},                  
        }
    },

}
const updateCategory = {
    body: {
        type: 'object',
        properties: {
            topic: {type: 'string'},
            ctype: {type: 'string'},
        }
    },
    params: {
        type: 'object',
        properties: {
          id: { type: 'string', format: 'uuid' }
        }
    }
}

const deleteCategory = {
    params: {
        type: 'object',
        properties: {
            id: {type: 'string', format: 'uuid'}
        }
    }
}
const newsbyid = {
     params: {
        type: 'object',
        properties: {
            id: {type: 'string', format: 'uuid'}
        }
    },

    response: {
        200: {
            type: 'array',
            items: {
                type: 'object',
                required: ['id', 'title', 'writer', 'content', 'createdate', 'status'],
                properties: {
                    id: {type: 'string',  format: 'uuid'},                                                              
                    title: {type: 'string'},
                    writer: {type: 'string'},
                    content: {type: 'string'},                                           
                    createdate:{type: 'string',format: "date-time"},                  
                    status: {type: 'string'},
                }
            }
        }
    },
   
}
const categorybyid = {
params: {
        type: 'object',
        properties: {
            id: {type: 'string', format: 'uuid'}
        }
    },
    response: {
        200: {
            type: 'array',
            items: {
                type: 'object',
                required: ['id', 'topic', 'ctype', 'createdate'],
                properties: {
                    id: {type: 'string',  format: 'uuid'},                                                              
                    topic: {type: 'string'},
                    ctype: {type: 'string'},                                       
                    createdate:{type: 'string',format: "date-time"},                  
                }
            }
        }
    },
   
}
const allPost = {
    response: {
        200: {
            type: 'array',
            items: {
                type: 'object',
                required: ['id', 'idarticle', 'idtopic'],
                properties: {
                    id: {type: 'string',  format: 'uuid'},                                                              
                    idarticle: {type: 'string',  format: 'uuid'}, 
                    idtopic: {type: 'string',  format: 'uuid'},    
                }
            }
        }
    }
}
const addPost = {
 body: {
        type: 'object',
        required: ['idarticle','idtopic'],
        properties: {
            idarticle: {type: 'string',  format: 'uuid'}, 
            idtopic: {type: 'string',  format: 'uuid'},   
    }
}
}
const updatePost = {
    body: {
        type: 'object',
        properties: {
            idarticle: {type: 'string',  format: 'uuid'}, 
            idtopic: {type: 'string',  format: 'uuid'},    
        }
    },
    params: {
        type: 'object',
        properties: {
          id: { type: 'string', format: 'uuid' }
        }
    }
}

const deletePost = {
    params: {
        type: 'object',
        properties: {
            id: {type: 'string', format: 'uuid'}
        }
    }
}
const Postbyid = {
params: {
        type: 'object',
        properties: {
            id: {type: 'string', format: 'uuid'}
        }
    },
    response: {
        200: {
            type: 'array',
            items: {
                type: 'object',
                required: ['id', 'idarticle', 'idtopic'],
                properties: {
                    id: {type: 'string',  format: 'uuid'},                                                              
                    idarticle: {type: 'string',  format: 'uuid'}, 
                    idtopic: {type: 'string',  format: 'uuid'},                  
                }
            }
        }
    },
}
 const findallwithparameter = {
params: {
        type: 'object',
        properties: {
            status: {type: 'string'},  
            topic: {type: 'string'},
        }
    },
    response: {
        200: {
            type: 'array',
            items: {
                type: 'object',
                 required: ['status', 'topic'],
                properties: {
                    title: {type: 'string'},
                    writer: {type: 'string'},
                    content: {type: 'string'},                                           
                    createdate:{type: 'string',format: "date-time"},                  
                    status: {type: 'string'},  
                    topic: {type: 'string'},
                    ctype: {type: 'string'},
                    idarticle: {type: 'string',  format: 'uuid'}, 
                    idtopic: {type: 'string',  format: 'uuid'},                                
                }
            }
        } }}
const updatestatus = {
    params: {
        type: 'object',
        properties: {
          id: { type: 'string', format: 'uuid' }
        }
    }
}
module.exports = {allNews, addNews, updateNews, deleteNews, allCategory, addCategory, updateCategory, deleteCategory,newsbyid,categorybyid, allPost, addPost, updatePost, deletePost,Postbyid,findallwithparameter,updatestatus}