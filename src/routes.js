const { v4: uuidv4 } = require('uuid');
const { allNews, addNews, updateNews, deleteNews, allCategory, addCategory, updateCategory, deleteCategory,newsbyid,categorybyid,allPost,addPost, updatePost, deletePost,Postbyid,findallwithparameter,updatestatus} = require('./schemas')
async function routes(fastify, options) {
    const client = fastify.db.client
    fastify.get('/api/getallnews/', {schema: allNews}, async function (request, reply) {
        try {
            const {rows} = await client.query('SELECT * FROM News')
            console.log(rows)
            return rows;
        } catch(err) {
            throw new Error(err)
        }
    })

    fastify.post('/api/insertnews/', {schema: addNews}, async function(request, reply) {
        const {title,writer,content} = request.body
        const id = uuidv4()
        const status='draft'
        createdate = new Date().toISOString()
        const query = {
            text: `INSERT INTO News (id, title,writer,content,"createdate",status)
                    VALUES($1, $2, $3, $4, $5, $6 ) RETURNING *`,
            values: [id, title, writer, content, createdate, status],
            }
        try {
            const {rows} = await client.query(query)
            console.log(rows[0])
            reply.send('Success Insert Data News with id :'+ id)
        } catch (err) {
            throw new Error(err)
        }
        
    })
    fastify.patch('/api/updatenews/:id',{schema: updateNews}, async function (request, reply) {
        const id = request.params.id
        const {title, writer, content} = request.body
        const query = {
            text:  `UPDATE News SET 
                    title = COALESCE($1, title), 
                    writer = COALESCE($2, writer), 
                    content = COALESCE($3, content) 
                    WHERE id = $4 RETURNING *`,
            values : [title, writer, content, id]
        }
        try {
            const {rows} = await client.query(query)
            console.log(rows[0])
            reply.send('Success update data with id : '+ id)
        } catch (err) {
            throw new Error(err)
        }
    } )
    fastify.delete('/api/deletenews/:id', {schema: deleteNews}, async function(request, reply) {
        console.log(request.params)
        try {
            const {rows} = await client.query('DELETE FROM News WHERE id = $1 RETURNING *', [request.params.id])
            console.log(rows[0])
            reply.send('Success delete datas with id : '+request.params.id)
        } catch(err) {
            throw new Error(err)
        }
    })
    fastify.get('/api/getallcategory/', {schema: allCategory}, async function (request, reply) {
        try {
            const {rows} = await client.query('SELECT * FROM Category')
            console.log(rows)
            return rows;
        } catch(err) {
            throw new Error(err)
        }
    })

    fastify.post('/api/insertcategory/', {schema: addCategory}, async function(request, reply) {
        const {topic,ctype} = request.body
        const id = uuidv4()
        createdate = new Date().toISOString()
        const query = {
            text: `INSERT INTO Category (id, topic,ctype,"createdate")
                    VALUES($1, $2, $3, $4 ) RETURNING *`,
            values: [id, topic, ctype, createdate],
            }
        try {
            const {rows} = await client.query(query)
            console.log(rows[0])
            reply.send('Success Insert Data News with id :'+ id)
        } catch (err) {
            throw new Error(err)
        }
        
    })
    fastify.patch('/api/updatecategory/:id',{schema: updateCategory}, async function (request, reply) {
        const id = request.params.id
        const {topic, ctype} = request.body
        const query = {
            text:  `UPDATE Category SET 
                    topic = COALESCE($1, topic), 
                    ctype = COALESCE($2, ctype) 
                    WHERE id = $3 RETURNING *`,
            values : [topic, ctype,  id]
        }
        try {
            const {rows} = await client.query(query)
            console.log(rows[0])
            reply.send('Success update data with id : '+ id)
        } catch (err) {
            throw new Error(err)
        }
    } )
    fastify.delete('/api/deletecategory/:id', {schema: deleteCategory}, async function(request, reply) {
        console.log(request.params)
         const id = request.params.id
        try {
            const {rows} = await client.query('DELETE FROM Category WHERE id = $1 RETURNING *', [request.params.id])
            console.log(rows[0])
            reply.send('Success delete datas with id : '+request.params.id)
        } catch(err) {
            throw new Error(err)
        }
    })
   fastify.get('/api/getnewsbyid/:id', {schema: newsbyid}, async function (request, reply) {
    console.log(request.params)
         const id = request.params.id
        try {
              const query = {
            text:  `SELECT * FROM News WHERE id = $1`,
            values : [id] }
            const {rows} = await client.query(query)
            console.log(rows)
            return rows;
        } catch(err) {
            throw new Error(err)
        }
    })
     fastify.get('/api/getcategorybyid/:id', {schema: categorybyid  }, async function (request, reply) {
    console.log(request.params)
         const id = request.params.id
        try {
              const query = {
            text:  `SELECT * FROM Category   WHERE id = $1`,
            values : [id] }
            const {rows} = await client.query(query)
            console.log(rows)
            return rows;
        } catch(err) {
            throw new Error(err)
        }
    })
fastify.get('/api/getallpost/', {schema: allPost}, async function (request, reply) {
        try {
            const {rows} = await client.query('SELECT * FROM Post')
            console.log(rows)
            return rows;
        } catch(err) {
            throw new Error(err)
        }
    })

    fastify.post('/api/insertpost/', {schema: addPost}, async function(request, reply) {
        const {idarticle,idtopic    } = request.body
        const id = uuidv4()
        const query = {
            text: `INSERT INTO Post  (id, idarticle, idtopic)
                    VALUES($1, $2, $3 ) RETURNING *`,
            values: [id, idarticle, idtopic],
            }
        try {
            const {rows} = await client.query(query)
            console.log(rows[0])
            reply.send('Success Insert Data News with id :'+ id)
        } catch (err) {
            throw new Error(err)
        }
        
    })
    fastify.patch('/api/updatepost/:id',{schema: updatePost  }, async function (request, reply) {
        const id = request.params.id
        const {idarticle, idtopic   } = request.body
        const query = {
            text:  `UPDATE Post SET 
                    idarticle = COALESCE($1, idarticle), 
                    idtopic = COALESCE($2, idtopic) 
                    WHERE id = $3 RETURNING *`,
            values : [idarticle, idtopic,  id]
        }
        try {
            const {rows} = await client.query(query)
            console.log(rows[0])
            reply.send('Success update data with id : '+ id)
        } catch (err) {
            throw new Error(err)
        }
    } )
    fastify.delete('/api/deletepost/:id', {schema: deletePost   }, async function(request, reply) {
        console.log(request.params)
         const id = request.params.id
        try {
            const {rows} = await client.query('DELETE FROM Post WHERE id = $1 RETURNING *', [request.params.id])
            console.log(rows[0])
            reply.send('Success delete datas with id : '+request.params.id)
        } catch(err) {
            throw new Error(err)
        }
    })
   fastify.get('/api/getpostbyid/:id', {schema: Postbyid}, async function (request, reply) {
    console.log(request.params)
         const id = request.params.id
        try {
              const query = {
            text:  `SELECT * FROM Post WHERE id = $1`,
            values : [id] }
            const {rows} = await client.query(query)
            console.log(rows)
            return rows;
        } catch(err) {
            throw new Error(err)
        }
    })



   fastify.get('/api/findallwithparam/:status/:topic',{schema: findallwithparameter  }, async function (request, reply) {
        console.log('request status lur '+ request.params.status)
        console.log('request topic lur '+ request.params.topic)
        const statusvalue = request.params.status
        const topicvalue = request.params.topic
        try {
              const query = {
            text:  `select title,writer,content,status,idarticle,idtopic, topic,ctype from Post inner join News on News.id = Post.idarticle inner join Category on Category.id=idtopic  where status=$1 and topic=$2`,
            values : [statusvalue,topicvalue] }
            const {rows} = await client.query(query)
            console.log(rows)
            return rows;
        } catch(err) {
            throw new Error(err)
        }
    })
   fastify.patch('/api/publish/:id',{schema: updatestatus}, async function (request, reply) {
        const id = request.params.id
        const query = {
            text:  `UPDATE News SET 
                    status = 'published'
                    WHERE id = $1 RETURNING *`,
            values : [ id]
        }
        try {
            const {rows} = await client.query(query)
            console.log(rows[0])
            reply.send('Success publish data with id : '+ id)
        } catch (err) {
            throw new Error(err)
        }
    } )
   fastify.patch('/api/trash/:id',{schema: updatestatus}, async function (request, reply) {
        const id = request.params.id
        const query = {
            text:  `UPDATE News SET 
                    status = 'deleted' 
                    WHERE id = $1 RETURNING *`,
            values : [ id]
        }
        try {
            const {rows} = await client.query(query)
            console.log(rows[0])
            reply.send('Success move data to trash with id : '+ id)
        } catch (err) {
            throw new Error(err)
        }
    } )
}

module.exports= routes