CREATE TABLE News (
    id UUID PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    writer VARCHAR(255) NOT NULL,
    content VARCHAR(255) NOT NULL,
    createdate TIMESTAMP NOT NULL,
    status VARCHAR(255) NOT NULL
);
CREATE TABLE Category(
    id UUID PRIMARY KEY,
    topic VARCHAR(255) NOT NULL,
    ctype VARCHAR(255) NOT NULL,
    createdate TIMESTAMP NOT NULL
);
CREATE TABLE Post(
    id UUID PRIMARY KEY,
    idarticle UUID,
    idtopic UUID
);

INSERT INTO News (id, title,writer,content,"createdate",status) 
VALUES ('54e694ce-6003-46e6-9cfd-b1cf0fe9d332', 'Pembunuhan seorang pria','Pembunuhan seorang pria di serang menimbulkan korban jiwa','anandadea', '2021-04-20T12:39:25Z', 'draft'); 
INSERT INTO Category (id, topic, ctype,  "createdate")  
VALUES ('d595655e-9691-4d1a-9a6b-9fbba046ae36', 'Pembunuhan', 'Tindakan asusila','2021-04-20T12:39:25Z');
INSERT INTO Post (id, idarticle,idtopic)  
VALUES ('c6ddcb30-93b1-440d-9021-5ab14816f7a6', '54e694ce-6003-46e6-9cfd-b1cf0fe9d332', 'd595655e-9691-4d1a-9a6b-9fbba046ae36');
